package week5.midterm.task3;

import static week5.midterm.task3.Balance.isBalanced;

public class Main {

    public static void main(String[] args) {
        Balance balance = new Balance();
        char exp[] = {'{','(',')','}','[',']'};
        if (isBalanced(exp))
            System.out.println("Yes");
        else
            System.out.println("No");
    }

}
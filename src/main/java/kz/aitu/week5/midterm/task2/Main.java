package week5.midterm.task2;

public class Main {

    public static void main(String[] args) {
        LinkedList list = new LinkedList();
        list.insertAtFirst(1);
        list.insertAtFirst(2);
        list.insertAtFirst(3);
        list.insertAtFirst(4);
        list.insertAtFirst(5);
        list.insertAtFirst(6);
        list.insertAtFirst(7);
        list.insertAtFirst(8);

        list.showList();
        list.removeAtPosition(2);
        list.showList();
    }
}
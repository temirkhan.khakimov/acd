package week5.midterm.task1;
//Answers for Test
//1) b
//2) a
//3) b
//4) d
//5) e
public class Main {
    public static void main(String[] args) {
        System.out.println("yyzzza: " + stringClean("yza"));
        System.out.println("abbbcdd: " + stringClean("abcd"));
        System.out.println("Hello: " + stringClean("Helo"));
    }

    public static String stringClean(String str) {
        for (int i = 1; i < (str.length()); i++) {
            if (str.charAt(i - 1) == str.charAt(i))
                return stringClean(str.substring(0, i - 1) + str.substring(i, str.length()));
        }
        return str;
    }
}
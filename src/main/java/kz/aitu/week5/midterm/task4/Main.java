package week5.midterm.task4;

public class Main {

    public static int Buy(int array[], int n) {
        int toBuy = array[0];

        for (int i = 1; i < n; i++)
            toBuy = Math.min(toBuy, array[i]);
        return toBuy;
    }

    public static int Sell(int array[], int n) {
        int sell = array[0];

        for (int i = 1; i < n; i++)
            sell = Math.max(sell, array[i]);
        return sell;
    }

    public static void main(String[] args) {

        int array[] = { 10, 7, 5, 8, 11 };
        int n = array.length;
        System.out.println( "Buy - "+ Buy(array, n));
        System.out.println( "Sell " + Sell(array, n));
    }

}
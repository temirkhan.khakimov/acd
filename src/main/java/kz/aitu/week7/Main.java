package kz.aitu.week7;

import java.util.Random;

public class Main {

    public static void main(String[] args) {
        int[] arr = new int[50];
        inputData(arr);

        BubbleSort bubble = new BubbleSort();
        Selection selection = new Selection();
        Insertion insertion = new Insertion();
        Merge merge = new Merge();
        QuickSort quick = new QuickSort();

        System.out.println("MY DATA");
        printAll(arr);

        System.out.println("BUBBLE SORTED=");
        bubble.bubblesot(arr);
        printAll(arr);

        System.out.println("SELECTION SORTED=");
        selection.sort(arr);
        printAll(arr);

        System.out.println("INSERTION SORTED=");
        insertion.insertionSort(arr);
        printAll(arr);

        System.out.println("MERGE SORTED=");
        merge.mergesort(arr);
        printAll(arr);

        System.out.println("QUICK SORTED");
        quick.quicksort(arr);
        printAll(arr);
    }

    private static void printAll(int[] array) {
        System.out.print("==> ");
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i]);
        }
        System.out.println();
    }

    private static void inputData(int[] array) {
        Random r = new Random();
        for (int i = 0; i < array.length; i++) {
            array[i] = r.nextInt(10);
        }
    }
}

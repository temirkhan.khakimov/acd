package endterm.occurence;

import java.util.Scanner;

public class FirstElement {

    public static int FirtstOccurence(int arr[], int x)
    {
       int left=0;
       int right=arr.length-1;
       int result=-1;

       while(left<=right){
           int mid=(left+right)/2;

           if(x==arr[mid]){
               result=mid;
               right=mid-1;
           }
           else if(x<arr[mid]){
               right=mid-1;
           }
           else{
               left=mid+1;
           }
       }
       return result;
    }

    public static void main (String[] args)
    {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int arr[] = new int[a];
        for (int i=0; i<a; i++){
            arr[i] = scanner.nextInt();
        }

        int x = scanner.nextInt();
        int index=FirtstOccurence(arr,x);

        if(index!=-1){
            System.out.println(index);
        }
        else{
            System.out.println("Netu");
        }

    }
}

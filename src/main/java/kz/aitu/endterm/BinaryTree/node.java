package endterm.BinaryTree;

public class Node {
    public Node left;
    int data;
    private int key;
    private Node left;
    protected Node right;

    public Node(int a){
        data=a;
        left=right=null;
    }

    public void setLeft(Node left) {
        this.left = left;
    }

    public void setRight(Node right) {
        this.right = right;
    }
}

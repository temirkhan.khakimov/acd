package kz.aitu.week4.queue;

public class Queue {
    private Node head;
    private Node tail;

    public Node getHead() {
        return head;
    }

    public void setHead(Node head) {
        this.head = head;
    }

    public void setTail(Node tail) {
        this.tail = tail;
    }

    public Node getTail() {
        return tail;
    }

    public void add(int value){
        Node newNode = new Node(value);
        if (head==null){
            head = newNode;
            tail = newNode;
        } else {
            tail.setNext(newNode);
            tail = newNode;
        }
    }

    public boolean empty(){
        if (head==null){
            return true;
        }
        return false;
    }

    public int size(){
        int a = 0;
        while (head!=null){
            head = head.getNext();
            a++;
        }
        return a;
    }
    
    public int peek() {
        if (head == null) {
            return 0;
        } else{
            return head.data;
        }
    }

    public Node poll(){
        Node previousHead = head;
        if(head == null) {
            return null;
        }else{
            head = head.next;
            return previousHead;
        }
    }
}

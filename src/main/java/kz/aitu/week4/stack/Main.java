package kz.aitu.week4.stack;

public class Main {
    public static void main(String[] args) {
        Stack stack = new Stack();
        Node top = stack.top();
        stack.push(new Node("2"));
        stack.push(new Node("2"));
        stack.push(new Node("3"));
        stack.push(new Node("4"));

        stack.pop();
        stack.pop();

        if (stack.empty()) System.out.println("Empty");
        else System.out.println("Not empty");

        System.out.println(stack.size());

    }
}

package kz.aitu.week4.stack;

public class Stack {
    private Node top;
    private int size = 0;

    public Stack(){
        this.top = new Node("null");
    }

    public Node top(){
        return top;
    }

    public void push(Node node){
        node.next = top;
        top = node;
        size++;
    }

    public void pop(){
        top=top.next;
        size--;
    }

    public int size(){
        return size;
    }

    public boolean empty(){
        if (top==null) return true;
        return false;
    }

}
package kz.aitu.week2;

import java.util.Scanner;

public class practice708 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        char [][] b = new char[a][a];
        for (int i=0;i<a;i++){
            for (int j=0;j<a;j++){
                b[i][j] = scanner.next().charAt(0);
            }
        }
        int c = scanner.nextInt();
        int d = scanner.nextInt();
        System.out.println(labirint(b, a, c-1, d-1));
    }

    public static int v = 0;

    public static int labirint(char b[][], int a, int c, int d){
        if (b[c][d] == '*') return 0;
        else v++;
        b[c][d] = '*';
        if (c+1 != a) labirint(b,a,c+1,d);
        if (c != 0) labirint(b,a,c-1,d);
        if (d+1 != a) labirint(b,a,c,d+1);
        if (d != 0) labirint(b,a,c,d-1);
        return v;
    }
}

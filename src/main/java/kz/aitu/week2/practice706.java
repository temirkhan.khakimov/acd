package kz.aitu.week2;

import java.util.Scanner;

public class practice706 {
    public static boolean labirint(char [][] c, int i, int j, int a, int b){
        if (c[i][j] == '#') return false;
        if (i==a-1 && j ==b-1){
            return true;
        }
        c[i][j] = '#';
        boolean flag = false;
        if (i+1 != a) flag = flag || labirint(c, i+1,j,a,b);
        if (i-1 != -1) flag = flag || labirint(c, i-1,j,a,b);
        if (j+1 != b)  flag = flag || labirint(c, i,j+1,a,b);
        if (j-1 != -1) flag = flag || labirint(c, i,j-1,a,b);
        return flag;
    }


    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        char [][] c = new char [a][b];
        for (int i= 0;i<a;i++) {
            for (int j = 0; j < b; j++) {
                c[i][j] = scanner.next().charAt(0);
            }
        }
        for (int i= 0;i<a;i++) {
            for (int j = 0; j < b; j++) {
                System.out.print(c[i][j]);
            }
            System.out.println();
        }

        if (labirint(c,0,0, a,b)){
            System.out.println("YES");
        } else {
            System.out.println("NO");
        }
    }
}
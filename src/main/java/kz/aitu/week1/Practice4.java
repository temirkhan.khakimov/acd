package kz.aitu.week1;

import java.util.Scanner;

public class Practice4 {
    public int findFactorial (int number)
    {
        if ( (number == 1) || (number == 0) )
            return 1;
        else
            return (number * findFactorial (number-1));
    }

    public void run() {
        int n;
        Scanner scanner = new Scanner(System.in);
        n = scanner.nextInt();
        System.out.println(findFactorial(n));
    }
}

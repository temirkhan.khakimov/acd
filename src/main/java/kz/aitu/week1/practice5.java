package kz.aitu.week1;

import java.util.Scanner;

public class Practice5 {

    public long findFibonacci( long n ){  
        if ( n == 0 || n == 1 )                                  
        return n; 
        else                                                   
        return fibonacci( n - 1 ) + fibonacci( n - 2 ); 
        
    } 


    public void run() {
        int n;
        Scanner scanner = new Scanner(System.in);
        n = scanner.nextInt();
        System.out.println(findFibonacci(n));

    }
}

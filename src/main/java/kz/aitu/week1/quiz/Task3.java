package kz.aitu.week1.quiz;

import java.util.Scanner;

public class Task3 {
    public static void recursion(int max1, int max2) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        if (n > 0) {
            if (max1 < n) {
                recursion(n, max1);
            } 
            else if (max2 < n) {
                recursion(max1, n);
            }
            else {
                recursion(max1, max2);
            }
        } else {
            System.out.println(max2);
        }
    }
    public static void main(String[] args) {
        recursion(0, 0);
    }
}

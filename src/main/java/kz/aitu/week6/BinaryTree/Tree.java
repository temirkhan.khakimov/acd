package kz.aitu.week6.BinaryTree;


public class Tree {
    Node root;

    Tree() {
        root = null;
    }

    public void insert( Integer key, String value) {
        Node node = new Node(key, value);
        if (root == null) {
            root = node;
        } else {
            Node current = root;
            while (current != null) {
                if (key < current.getKey()) {
                    if (current.getLeft() == null) {
                        current.setLeft(node);
                        return;
                    }
                    current = current.getLeft();
                } else if (key > current.getKey()) {
                    if (current.getRight() == null) {
                        current.setRight(node);
                        return;
                    }
                    current = current.getRight();

                }
            }

        }
    }



    public boolean delete() {
        return true;
    }
    
    public String find(Integer key) {
        Node node = findNode(root, key);
        if(node == null) return null;
        else return node.getValue();
    }

    private Node findNode(Node node, Integer key) {
        if(node == null) return null;
        if(key > node.getKey()) return findNode(node.getRight(), key);
        else if(key < node.getKey()) return findNode(node.getLeft(), key);
        else return node;
    }
    
    public void deleteKey(int key){ 
        root = deleteRec(root, key); 
    }
    
    Node deleteRec(Node root, int key){ 
        if (root == null)  return root; 
  
        if (key < root.key) 
            root.left = deleteRec(root.left, key); 
            
        else if (key > root.key) 
            root.right = deleteRec(root.right, key); 
            
        else{ 
            if (root.left == null) 
                return root.right; 
            else if (root.right == null) 
                return root.left; 
                
            root.key = minValue(root.right); 
            root.right = deleteRec(root.right, root.key); 
        } 
  
        return root; 
    } 

    public void printAll() {
        Node node = root;
        while (node != null) {
            if (node.getLeft() != null) {
                node = node.getLeft();
                return;

            }
            System.out.println(node.getValue());
            if (node.getRight() != null) {
                node = node.getRight();
            }
            return;
        }
        return;
    }
}

package kz.aitu.week6.BinaryTree;


public class Node {
    private int key;
    private String value;
    private Node right;
    private Node left;

    public Node(Integer key, String value) {
        this.key = key;
        this.value = value;
        this.right = null;
        this.left = right;
    }
    
    public Node(Integer key) {
        this.key = key;
    }
    public void setRight(Node right) {
        this.right = right;
    }

    public int getKey() {
        return key;
    }

    public void setKey(Integer key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Node getRight() {
        return right;
    }
    public Node getLeft() {
        return left;
    }
    public void setLeft(Node left) {
        this.left = left;
    }
}
